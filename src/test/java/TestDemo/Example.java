package TestDemo;
import org.junit.jupiter.*;
import org.junit.jupiter.api.*;

import Demo.Oddeven;
import Demo.Prime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.*;
public class Example {
	
	static Oddeven obj;
	static Prime obj1;
	@BeforeAll
	public static void abc1(){
		obj=new Oddeven();
		obj1=new Prime();
		System.out.println("I am BeforeAll");
	}
	@BeforeEach
	public void abc3(){
		System.out.println("I am BeforeEach");
	}
	
	@Test
	public void abc8(){
		assertTrue(obj.evenodd(4),"Even");
		assertFalse(obj.evenodd(7),"False");
		assertEquals(obj.evenodd(100),true);
		assertEquals(obj1.Primeornot(100),false);
		System.out.println("I am TestCase");
	}
	
	@Test
	public void abc4(){
		//assertNull(obj.evenodd(0));
		assertNotNull(obj.evenodd(1));
		assertEquals(obj.evenodd(101),false);
		assertEquals(obj1.Primeornot(101),true);
		System.out.println("I am Testcase");
	}
	@AfterEach
	public void abc5(){
		System.out.println("I am AfterEach");
		
	}
	@AfterAll
	public static void abc6(){
		System.out.println("I am AfterAll");
	}

}
